import javafx.fxml.FXML
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.input.KeyEvent
import javafx.scene.layout.VBox
import tornadofx.View
import java.util.*


class Calculator : View() {
    override val root: VBox by fxml()

    @FXML
    lateinit var display: Label

    var operatorEqualsClicked = false;

    init {
        title = "Kalkulator"

        root.lookupAll(".button").forEach { b ->
            b.setOnMouseClicked {
                calculate((b as Button).text)
            }
        }

        root.addEventFilter(KeyEvent.KEY_TYPED) {
            calculate(it.character.toUpperCase().replace("\r", "="))
        }

    }

    private fun calculate(x: String) {
        if (operatorEqualsClicked) {
            display.text = ""
            operatorEqualsClicked = false
        }
        when {
            Regex("[=]").matches(x) -> {
                val convertedToPostfix = convertInfixToPostfix(display.text)
                display.text = onpCalculate(convertedToPostfix)
                operatorEqualsClicked = true
            }
            Regex("C").matches(x) -> {
                display.text = ""
            }
            else -> {
                display.text += x
            }
        }
    }

    private fun convertInfixToPostfix(expr: String): String {
        println("Infix: $expr")
        var result = "";
        val stack = Stack<Char>();
        for (i in expr.indices) {
            if (precedence(expr[i]) > 0) {
                result += " "
                while (!stack.isEmpty() && precedence(stack.peek()) >= precedence(expr[i])) {
                    result += stack.pop() + " ";
                }
                stack.push(expr[i])
            } else {
                result += expr[i]
            }
        }
        var i = 0;
        val stackSize = stack.size
        while (i < stackSize) {
            result += " " + stack.pop();
            i++
        }
        return result
    }

    private fun precedence(char: Char): Int {
        when (char) {
            '+' -> return 1
            '-' -> return 2
            '*' -> return 3
            '/' -> return 4
        }
        return -1;
    }

    private fun onpCalculate(expr: String): String {
        println("Postfix: $expr")
        if (expr.isEmpty()) throw IllegalArgumentException("Expresssion cannot be empty")
        val tokens = expr.split(' ').filter { it != "" }
        val stack = mutableListOf<Double>()
        for (token in tokens) {
            val d = token.toDoubleOrNull()
            if (d != null) {
                stack.add(d)
            } else if ((token.length > 1) || (token !in "+-*/")) {
                throw IllegalArgumentException("$token is not a valid token")
            }
            else if (stack.size < 2) {
                throw IllegalArgumentException("Stack contains too few operands")
            } else {
                val d1 = stack.removeAt(stack.lastIndex)
                val d2 = stack.removeAt(stack.lastIndex)
                stack.add(
                    when (token) {
                        "+" -> d2 + d1
                        "-" -> d2 - d1
                        "*" -> d2 * d1
                        else -> d2 / d1
                    }
                )
            }
        }
        return stack[0].toString();
    }
}